# Ansible ispconfig

An experimentation of using ansible to setup ispconfig in a server. The system will use lxc containers and various services would be setup as follows:

- main server (physical): ssh + dns + ispconfig panel (private ip address: 10.0.0.1)
- web server (lxc container): apache (private ip address: 10.0.0.2)
- db server (lxc container): mysql (private ip address: 10.0.0.3)
- mail server (lxc container): dovecot + postfix (private ip address: 10.0.0.4)
- app server (lxc container): php-fpm (private ip address: 10.0.0.5)

We assume that all servers will run debian buster.
